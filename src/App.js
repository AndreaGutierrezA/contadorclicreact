import './App.css';
import Boton from './componentes/Boton';
import Contador from './componentes/Contador';
import freecodeCampLogo from './imagenes/freecodecamp-logo.png'
// useState es un hoocks que permite agregarle un ESTADO  aun COMPONENTE FUNCIONAL
import { useState } from 'react';

// componente App
function App() {
  /* Se declara una constante que sera un ARREGLO con DOS ELEMENTOS
    numClicsEstado -> es el valor que se quiere usar como el ESTADO
    setNumClics -> FUNCION que permitira actualizar el estado, se usa el set=Asignar
    useState = es el hoock y es quien retonara un valor, el valor inicial del ESTADO es 0
    
    ejemplo: const [color, setColor] = useState(0);*/

  const [numClicsEstado, setNumClics] = useState(0);

  // estas dos const se pasan como props al componente Boton
  /* Definir una función, se crea una funcion y se le asignaa a la constante manejarClic*/
   const manejarClicFuncion = () => {
     // cuando se da Clic en el boton CLIC , lo va a para es que el estado 'numClicsEstado' aumentara en 1 -> numClics + 1
     // se usa la funcion setNumClics y se le pasa como argumento 'numClicsEstado + 1', lo cual sera el nuevo estado del componente
    setNumClics(numClicsEstado + 1);
     console.log('Clic');
   }
    /* se crea una funcion y se le asignaa a la constante reinciarContador*/
   const reinciarContadorFuncion  = () => {
     // reiniciar el estado
    setNumClics(0);
     console.log('Reinciar');
   }


  return (
    <div className="App">
      {/* contenedor del logo*/}
      <div className="freecodecamp-logo-contenedor">
        <img className="freecodecamp-logo" src={freecodeCampLogo} alt="Logo de freeCodeCamp" />
      </div>
      {/* contador va ser un componente y los botones otro */}
      <div className="contenedor-contador">
        {/* renderizar o crear el componente contador, el necesita de ciertos props que en este caso es numClics que tendra como valor el numClicsEstado que es el ESTADO de arriba*/}
      <Contador numClics={numClicsEstado} />
        {/* renderizar o crear los botones, ellos necesita de ciertos props que son texto, esBotonDeClic, manejarClic*/}
        <Boton 
          texto='Clic'
          esBotonDeClic={true}
          manejarClic={manejarClicFuncion} />
        <Boton 
          texto='Reiniciar'
          esBotonDeClic={false}
          manejarClic={reinciarContadorFuncion} />
        
      </div>
    </div>
  );
}

export default App;