import React from 'react';
import '../hoja-de-estilo/Boton.css';

/* creacion del componente del Boton */
/* props propiedad, en este caso recibe 3 props*/
/* Desestructuracion { texto, esBotonDeClic } esta son propiedades que hacen parte de props,
   es solo que nos vamos a referir a texto y a esBotonDeClic.... para no usar La PALABRA props*/

/* clase basa en condicion para usarla en el boton ya que son botones que realizarana acciones diferentes */
/* Bonton de hacer Clic y de Reiniar */

/* esBotonDeClic de tipo boolean..dependiendo de su valor se le asigna una clase,
para ello se usa el OPERADOR TERNARIO
boton-clic y boton-reinicar son clases, si esBotonDeClic es true entopnces className = 'boton-clic' 
sino className = 'boton-reinicar'*/

/* Event Listener el escucha los eventos => onClick..
manejarClic es una funcion que realizara la funcion de lo q debe hacer el boton*/

/* manejarClic es una funcion y viene del componente que renderice o crre el boton en este caso es 
App.js */
function Boton({ texto, esBotonDeClic, manejarClic }) {
    return (
      <button
        className={esBotonDeClic ? 'boton-clic' : 'boton-reiniciar'}
        onClick={manejarClic}>
        {texto}
      </button>
    );
  }
export default Boton;