import React from 'react';
import '../hoja-de-estilo/Contador.css';

// definicion o creacion del componente Contador
/* props propiedad, en este caso recibe 1 props -> numClics*/
function Contador({ numClics }) {
  return (
    <div className='contador-estilo'>
      {numClics}
    </div>
  );
}

export default Contador;